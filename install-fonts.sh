#!/bin/bash

# Install default Office fonts
# see: https://helpcenter.onlyoffice.com/de/server/linux/document/install-fonts.aspx
echo "Installing Fonts"

sudo mkdir -p /usr/share/fonts/truetype
sudo mkdir -p /var/www/onlyoffice/documentserver/core-fonts/

sudo apt update
sudo apt -y install unzip
sudo apt -y install fontconfig # for fc-cache

function get_font {
    wget "http://legionfonts.com/download/$1"
    unzip "$1"
    sudo cp "$2.ttf" /usr/share/fonts/truetype
    sudo cp "$2.ttf" /var/www/onlyoffice/documentserver/core-fonts/
    rm "$2.ttf"
    rm "$1"
}

# Download and install fonts
get_font arial Arial
get_font calibri Calibri
get_font courier-new 'Courier New'
get_font symbol Symbol
get_font times-new-roman 'Times New Roman'
get_font tw-cen-mt 'Tw Cen MT'
get_font tw-cen-mt-condensed 'Tw Cen MT Condensed'
get_font cambria Cambria

# Install Montserrat.zip
unzip Montserrat.zip
sudo cp Montserrat*.ttf /usr/share/fonts/truetype
sudo cp Montserrat*.ttf /var/www/onlyoffice/documentserver/core-fonts/
rm Montserrat*

# Install OpenDyslexic2
sudo cp OpenDyslexic*.ttf /usr/share/fonts/truetype
sudo cp OpenDyslexic*.ttf /var/www/onlyoffice/documentserver/core-fonts/
rm OpenDyslexic*

# Refresh the cache
sudo fc-cache -fv

# OnlyOffice-provided script
/usr/bin/documentserver-generate-allfonts.sh

exit 0
