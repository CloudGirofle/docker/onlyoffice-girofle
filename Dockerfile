FROM quay.io/libre.sh/onlyoffice:v8.1.3.3

## Add fonts
RUN mkdir -p /scripts
COPY install-fonts.sh /scripts
COPY Montserrat.zip /scripts
COPY OpenDyslexic2 /scripts
WORKDIR /scripts
RUN chmod +x install-fonts.sh
RUN ./install-fonts.sh

## Allow external IPs to the /info mountpoint
ARG dsdoc="/etc/onlyoffice/documentserver/nginx/includes/ds-docservice.conf"
RUN echo "CAREFUL we totally replace file $dsdoc"
COPY ds-docservice.conf /etc/onlyoffice/documentserver/nginx/includes/ds-docservice.conf
