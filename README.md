# OnlyOffice-Girofle
The version of OnlyOffice deployed by [Cloud Girofle](https://girofle.cloud)

A slightly custom build of OnlyOffice :
- Based on `libresh/onlyoffice` (assembled by [IndieHosters](indiehosters.net)):
  - with an unlimited number of concurrent connections
  - with legacy mobile edit activated
- With additional fonts installed:
  - [Montserrat](https://fonts.google.com/specimen/Montserrat) (Google, Open Font license)
  - [OpenDyslexic2](https://opendyslexic.org/) (SIL-OFL, SIL-OFL license)
